package com.gabchak.task6.controller;

@FunctionalInterface
public interface Print {
    void print();
}
