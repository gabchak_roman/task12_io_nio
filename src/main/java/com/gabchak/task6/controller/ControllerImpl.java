package com.gabchak.task6.controller;

import com.gabchak.task6.model.Model;
import com.gabchak.task6.view.ConsoleReader;
import com.gabchak.task6.view.View;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Model model;
    private Map<String, Print> commands = new HashMap<>();

    public ControllerImpl(View view, ConsoleReader consoleReader, Model model) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.model = model;
    }

    @Override
    public void start() {
        view.printMessage(
                "Commands: 'cd..' - back to, 'cd <directory>' - forward to, "
                        + "'dir' - show directory, '<drive>:' - switch drive,"
                        + " 'cls' - clean console.");
        setDirectory();
    }


    private void printFolderContent() {
        try {
            view.printYellowMessage("Directory of " + model.getPath());
        } catch (IOException e) {
            view.printRedMessage("Property value cannot be accessed.");
        }
        view.printFolderContent(model.getFolderContent());
    }

    private void setDirectory() {
        setCommands();
        while (true) {
            printFolderRoot();
            String key = consoleReader.readLine().trim();
            if (commands.containsKey(key)) {
                commands.get(key).print();
            } else if (key.length() > 3 && key.substring(0, 3).equals("cd ")) {
                cdForwardTo(key);
            } else if (key.length() == 2 && key.substring(1, 2).equals(":")) {
                setDrive(key);
            } else {
                view.printRedMessage(
                        "'" + key +
                        "' is not recognized as an internal or external command,\n" +
                        "operable program or batch file.");
            }
        }
    }

    private void setCommands() {
        commands.put("dir", this::printFolderContent);
        commands.put("", this::setDirectory);
        commands.put("exit", this::printFolderRoot);
        commands.put("cls", () -> view.printMessage("\n".repeat(50)));
        commands.put("cd..", () -> model.backTo());
    }

    private void cdForwardTo(String key) {
        try {
            model.forwardTo(key.trim().substring(3));
        } catch (Exception e) {
            view.printRedMessage(e.getMessage());
        }
    }

    private void setDrive (String key){
        try {
            model.setDrive(key + "\\");
        } catch (Exception e) {
            view.printRedMessage(e.getMessage());
        }
    }

    private void printFolderRoot() {
        try {
            view.printYellowMessage(
                    model.getPath() + ">");
        } catch (IOException e) {
            view.printRedMessage("Property value cannot be accessed.");
        }
    }


}
