package com.gabchak.task6.model;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface Model {

    String getPath() throws IOException;

    void setDrive(String drive) throws Exception;

    List<File> getFolderContent();

    void backTo();

    void forwardTo(String folderName) throws Exception;
}
