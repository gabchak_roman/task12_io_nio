package com.gabchak.task6.model;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

public class ModelImpl implements Model {

    private File file = new File(FileSystemView
            .getFileSystemView()
            .getHomeDirectory()
            .getParent());

    @Override
    public String getPath() throws IOException {
        return file.getCanonicalPath();
    }

    @Override
    public void setDrive(String drive) throws Exception {
        File fileDrive = new File(drive.trim().toUpperCase());
        if (fileDrive.exists()) {
            file = new File(fileDrive.getCanonicalPath());
        }
        else {
            throw new Exception("The system cannot find the drive specified.");
        }
    }

    @Override
    public List<File> getFolderContent() {
        return Arrays.asList(
                Objects.requireNonNull(
                        file.listFiles()));
    }

    @Override
    public void backTo() {
        if (file.getParent() != null) {
            file = new File(file.getParent());
        }
    }

    @Override
    public void forwardTo(String folderName) throws Exception {
        AtomicBoolean res = new AtomicBoolean(true);
        for (File f : getFolderContent()) {
            if (f.getName().equalsIgnoreCase(folderName)) {
                String path = file.getPath() + "\\" + f.getName();
                boolean isDirectory = new File(path).isDirectory();
                if (isDirectory) {
                    file = new File(path);
                    res.set(false);
                    break;
                }
            }
        }
        if (res.get()) {
            throw new Exception("The system cannot find the path specified.");
        }
    }
}
