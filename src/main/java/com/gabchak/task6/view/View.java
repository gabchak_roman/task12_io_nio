package com.gabchak.task6.view;

import java.io.File;
import java.util.List;

public interface View {

    void printMessage(String message);

    void printYellowMessage(String message);

    void printRedMessage(String message);

    void printFolderContent (List<File> files);
}
