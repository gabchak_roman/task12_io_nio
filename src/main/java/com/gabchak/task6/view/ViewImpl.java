package com.gabchak.task6.view;

import java.io.File;
import java.util.List;

public class ViewImpl implements View {
    public static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

     @Override
    public void printYellowMessage(String message) {
        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }

    @Override
    public void printRedMessage(String message) {
        System.out.println(ANSI_RED + message + ANSI_RESET);
    }

    @Override
    public void printFolderContent(List<File> files){
            files.forEach(file -> System.out.println("<dir>  ->          "+ file.getName()));
    }
}
