package com.gabchak.task6;

import com.gabchak.task6.controller.Controller;
import com.gabchak.task6.controller.ControllerImpl;
import com.gabchak.task6.model.ModelImpl;
import com.gabchak.task6.view.ConsoleReader;
import com.gabchak.task6.view.ViewImpl;

public class Main {
    public static void main(String[] args) {

        Controller controller = new ControllerImpl(
                new ViewImpl(),
                new ConsoleReader(),
                new ModelImpl()
        );

        controller.start();

    }
}
