package com.gabchak.task2.model;


import com.gabchak.task2.model.data.Ship;

import java.io.IOException;

public interface Model {

    Ship getShip();

    void serialize() throws IOException;

    Ship deserialize() throws IOException, ClassNotFoundException;
}
