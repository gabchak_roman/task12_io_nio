package com.gabchak.task2.model.data;

import java.io.Serializable;

public class Droid implements Serializable {

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    private String name;
    private int age;
    private transient int batterySize;

    public Droid(String name, int age, int batterySize) {
        this.name = name;
        this.age = age;
        this.batterySize = batterySize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getBatterySize() {
        return batterySize;
    }

    public void setBatterySize(int batterySize) {
        this.batterySize = batterySize;
    }

    @Override
    public String toString() {
        return ANSI_YELLOW + "\n" +
                "name: '" + ANSI_RESET + name + '\'' +
                ANSI_YELLOW + ", age: " + ANSI_RESET + age +
                ANSI_YELLOW + ", batterySize: " + ANSI_RESET + batterySize;
    }
}
