package com.gabchak.task2.model.data;

import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DroidsList {

        private List<Droid> droidsList = new ArrayList<Droid>();

    public DroidsList() {
        addSomeDroids();
    }

    public List<Droid> getDroidsList() {
        return droidsList;
    }

    private void addSomeDroids(){
        for (int i = 0; i < 10; i++) {
            droidsList.add(
                    new Droid(new Faker(new Random()).funnyName().name(),
                    new Random().nextInt(300),
                    new Random().nextInt(6000)));
        }
    }
}
