package com.gabchak.task2.model.data;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {

    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    private String maker;
    private String name;
    private String model;
    private transient int year;
    private int capacity;
    private List<Droid> droid;

    public Ship(String maker, String name, String model,
                int year, int capacity, List<Droid> droid) {
        this.maker = maker;
        this.name = name;
        this.model = model;
        this.year = year;
        this.capacity = capacity;
        this.droid = droid;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setDroids(List<Droid> droids) {
        this.droid = droids;
    }

    @Override
    public String toString() {
        return ANSI_YELLOW + "Ship " +
                "maker: " + ANSI_RESET + maker +
                ANSI_YELLOW + ", name: " + ANSI_RESET + name +
                ANSI_YELLOW + ", model: " + ANSI_RESET + model +
                ANSI_YELLOW + ", year: " + ANSI_RESET + year +
                ANSI_YELLOW + ", capacity : " + ANSI_RESET + capacity +
                ANSI_YELLOW + ",\nDroids : " + ANSI_RESET + droid;
    }
}
