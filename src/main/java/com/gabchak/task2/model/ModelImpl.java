package com.gabchak.task2.model;

import com.gabchak.task2.model.data.Ship;

import java.io.*;

public class ModelImpl implements Model{

    private Ship ship;

    public ModelImpl(Ship ship) {
        this.ship = ship;
    }

    @Override
    public Ship getShip() {
        return ship;
    }

    @Override
    public void serialize() throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream("Ship");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(ship);
        objectOutputStream.close();
    }


    @Override
    public Ship deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("Ship");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Ship shipDeserialized = (Ship) objectInputStream.readObject();
        objectInputStream.close();
        return shipDeserialized;
    }

}
