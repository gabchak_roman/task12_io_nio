package com.gabchak.task2.view;

public class ViewImpl implements View{
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }
}
