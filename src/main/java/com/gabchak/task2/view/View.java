package com.gabchak.task2.view;

public interface View {

    void printMessage(String message);

}
