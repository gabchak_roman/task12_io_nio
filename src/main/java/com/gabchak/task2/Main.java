package com.gabchak.task2;

import com.gabchak.task2.controller.Controller;
import com.gabchak.task2.controller.ControllerImpl;
import com.gabchak.task2.model.ModelImpl;
import com.gabchak.task2.model.data.DroidsList;
import com.gabchak.task2.model.data.Ship;
import com.gabchak.task2.view.ConsoleReader;
import com.gabchak.task2.view.ViewImpl;

public class Main {
    public static void main(String[] args) {

        Ship ship = new Ship("Silent", "Felicia", "SF19",
                2019, 1000, new DroidsList().getDroidsList());

        Controller controller = new ControllerImpl(
                new ViewImpl(),
                new ConsoleReader(),
                new ModelImpl(ship)
        );

        controller.start();

    }
}
