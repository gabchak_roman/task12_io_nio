package com.gabchak.task2.controller;

@FunctionalInterface
public interface Print {
    void print();
}
