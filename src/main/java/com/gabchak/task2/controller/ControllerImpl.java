package com.gabchak.task2.controller;

import com.gabchak.task2.model.Model;
import com.gabchak.task2.view.ConsoleReader;
import com.gabchak.task2.view.View;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Model model;
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public ControllerImpl(View view, ConsoleReader consoleReader, Model model) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.model = model;
    }

    @Override
    public void start() {
        menu();
        print();
    }

    private void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", " 1  -  Print ship                      ");
        menu.put("2", " 2  -  Serialize                       ");
        menu.put("3", " 3  -  Print deserialized ship         ");
        menu.put("Q", " Q  -  Exit                            ");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        view.printMessage(model.getShip().toString());
    }

    private void pressButton2() {
        try {
            model.serialize();
        } catch (IOException e) {
            view.printMessage("File not found");
        }
    }

    private void pressButton3() {
        try {
            view.printMessage(model.deserialize().toString());
        } catch (IOException | ClassNotFoundException e) {
            view.printMessage("File not found");
        }
    }

    private void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select menu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMessage("\nMENU:");
        menu.values().forEach(
                message -> view.printMessage(message));
    }
}
