package com.gabchak.task5.view;

import java.util.List;

public interface View {

    void printMessage(String message);

    void printYellowMessage(String message);

    void printList(List<String> list);
}
