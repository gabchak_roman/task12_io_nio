package com.gabchak.task5.view;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ViewImpl implements View {
    public static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void printYellowMessage(String message) {
        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }

    @Override
    public void printList(List<String> list) {
        AtomicInteger number = new AtomicInteger(1);
        list.forEach(v -> System.out.println(number.getAndIncrement() + " : "
                + ANSI_GREEN + v + ANSI_RESET));
    }
}
