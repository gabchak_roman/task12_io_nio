package com.gabchak.task5.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ModelImpl implements Model{

    @Override
    public List<String> readCommentsFromJavaSourceCode(String filePath) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        List<String> comments = new ArrayList<>();
        String line;

        while ((line = reader.readLine()) != null) {
            line = line.trim();
            if (line.startsWith("//")) {
                comments.add(line);
            } else if (line.startsWith("/*")) {
                StringBuilder comment = new StringBuilder();
                comment.append(line);
                while (!(line=reader.readLine()).trim().startsWith("*/")) {
                    comment.append("\n").append(line);
                }
                comment.append("\n").append(line);
                comments.add(comment.toString());
            }
        }
        reader.close();
        return comments;
    }
}
