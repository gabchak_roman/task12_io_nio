package com.gabchak.task5.model;

import java.io.IOException;
import java.util.List;

public interface Model {
    List<String> readCommentsFromJavaSourceCode(String filePath) throws IOException;
}
