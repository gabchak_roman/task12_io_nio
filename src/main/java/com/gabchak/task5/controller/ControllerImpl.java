package com.gabchak.task5.controller;

import com.gabchak.task5.model.Model;
import com.gabchak.task5.view.ConsoleReader;
import com.gabchak.task5.view.View;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Model model;
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public ControllerImpl(View view, ConsoleReader consoleReader, Model model) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.model = model;
    }

    @Override
    public void start() {
        menu();
        print();
    }

    private void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", " 1  -  To read the comments from the Java source code.                ");
        menu.put("Q", " Q  -  Exit.                                                          ");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);

    }

    private void pressButton1() {
        view.printYellowMessage("Write the path of the file to read:"
                + "\n(example: \"C:/task12_io_nio/src/main/java/task5/Main.java\")");
        try {
            view.printList(model.readCommentsFromJavaSourceCode(
                    new ConsoleReader().readLine()
            ));
        } catch (IOException e) {
            view.printMessage("File not found!");
        }
    }

    private void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select menu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMessage("\nMENU:");
        menu.values().forEach(
                message -> view.printMessage(message));
    }
}
