package com.gabchak.task5.controller;

@FunctionalInterface
public interface Print {
    void print();
}
