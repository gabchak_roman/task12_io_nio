package com.gabchak.task5;

import com.gabchak.task5.controller.Controller;
import com.gabchak.task5.controller.ControllerImpl;
import com.gabchak.task5.model.ModelImpl;
import com.gabchak.task5.view.ConsoleReader;
import com.gabchak.task5.view.ViewImpl;

public class Main {

public static void main(String[] args) {

        Controller controller = new ControllerImpl(
        new ViewImpl(),
        new ConsoleReader(),
        new ModelImpl()
        );
        controller.start();

        }
}