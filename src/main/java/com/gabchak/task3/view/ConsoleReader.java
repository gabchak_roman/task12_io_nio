package com.gabchak.task3.view;

import java.util.Scanner;

public class ConsoleReader {
    private Scanner input = new Scanner(System.in);

    public String readLine() {
        return input.nextLine();
    }
}