package com.gabchak.task3.view;

public interface View {

    void printMessage(String message);

    void printYellowMessage(String message);
}
