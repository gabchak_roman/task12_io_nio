package com.gabchak.task3.view;

public class ViewImpl implements View {
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void printYellowMessage(String message) {
        System.out.println(ANSI_YELLOW + message + ANSI_RESET);
    }
}
