package com.gabchak.task3.model;


import java.io.*;

public class ModelImpl implements Model {

    private File file;

    public ModelImpl(File file) {
        this.file = file;
    }

    //------------------------------------------------------------------------------------------
    @Override
    public String bufferReaderWriter() throws IOException {
        double start = System.currentTimeMillis();
        int counter = 0;
        int res = 0;
        DataInputStream dataInputStream =
                new DataInputStream(
                        new BufferedInputStream(
                                new FileInputStream(file)));
        while ((res = dataInputStream.read()) != -1) {
            counter++;
        }
        dataInputStream.close();
        double end = System.currentTimeMillis();
        return "Buffered reader. "
                + "\ntime: " + (end - start) + " ms"
                + "\ncounter: " + counter;
    }

    //------------------------------------------------------------------------------------------
    @Override
    public String bufferReaderWriterWithSetBuffer(int bufferSizeMB) throws IOException {
        bufferSizeMB = bufferSizeMB * 1024 * 1024;
        double start = System.currentTimeMillis();
        int counter = 0;
        int res = 0;
        DataInputStream dataInputStream =
                new DataInputStream(
                        new BufferedInputStream(
                                new FileInputStream(file), bufferSizeMB));
        while ((res = dataInputStream.read()) != -1) {
            counter++;
        }
        dataInputStream.close();
        double end = System.currentTimeMillis();
        return "Buffered reader with buffer size: " + bufferSizeMB
                + "\ntime: " + (end - start) + " ms"
                + "\ncounter: " + counter;
    }

    //------------------------------------------------------------------------------------------
    @Override
    public String inputStreamReader() throws IOException {
        double start = System.currentTimeMillis();
        int counter = 0;
        int res = 0;
        InputStream inputStream = new FileInputStream(file);
        while ((res = inputStream.read()) != -1) {
            counter++;
        }
        inputStream.close();
        double end = System.currentTimeMillis();
        return "Usual reader."
                + "\ntime: " + (end - start) + " ms"
                + "\ncounter: " + counter;
    }

}
