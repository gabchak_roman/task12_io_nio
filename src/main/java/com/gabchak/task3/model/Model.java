package com.gabchak.task3.model;

import java.io.IOException;

public interface Model {

    //------------------------------------------------------------------------------------------
    String bufferReaderWriter() throws IOException;

    //------------------------------------------------------------------------------------------
    String bufferReaderWriterWithSetBuffer(int bufferSizeMB) throws IOException;

    //------------------------------------------------------------------------------------------
    String inputStreamReader() throws IOException;
}
