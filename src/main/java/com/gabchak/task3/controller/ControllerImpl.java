package com.gabchak.task3.controller;

import com.gabchak.task3.model.Model;
import com.gabchak.task3.view.ConsoleReader;
import com.gabchak.task3.view.View;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerImpl implements Controller {

    private View view;
    private ConsoleReader consoleReader;
    private Model model;
    private Map<String, String> menu;
    private Map<String, Print> methodsMenu;

    public ControllerImpl(View view, ConsoleReader consoleReader, Model model) {
        this.view = view;
        this.consoleReader = consoleReader;
        this.model = model;
    }

    @Override
    public void start() {
        menu();
        print();
    }

    private void menu() {
        menu = new LinkedHashMap<>();

        menu.put("1", " 1  -  Read the file using buffered reader.                          ");
        menu.put("2", " 2  -  Read the file using buffered reader with set buffer size.     ");
        menu.put("3", " 3  -  Read the file using usual reader                              ");
        menu.put("Q", " Q  -  Exit                                                          ");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
    }

    private void pressButton1() {
        try {
            view.printYellowMessage(model.bufferReaderWriter());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        view.printYellowMessage("Set buffer size MB: ");
        try {
            view.printYellowMessage(
                    model.bufferReaderWriterWithSetBuffer(
                            Integer.parseInt(new ConsoleReader().readLine())
                    ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton3() {
        try {
            view.printYellowMessage(model.inputStreamReader());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select menu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        view.printMessage("\nMENU:");
        menu.values().forEach(
                message -> view.printMessage(message));
    }
}
