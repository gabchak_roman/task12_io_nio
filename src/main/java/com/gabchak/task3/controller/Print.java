package com.gabchak.task3.controller;

@FunctionalInterface
public interface Print {
    void print();
}
