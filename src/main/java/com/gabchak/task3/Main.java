package com.gabchak.task3;

import com.gabchak.task3.controller.Controller;
import com.gabchak.task3.controller.ControllerImpl;
import com.gabchak.task3.model.ModelImpl;
import com.gabchak.task3.view.ConsoleReader;
import com.gabchak.task3.view.ViewImpl;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        File file = new File("textToRead.pdf");

        Controller controller = new ControllerImpl(
                new ViewImpl(),
                new ConsoleReader(),
                new ModelImpl(file)
        );

        controller.start();
    }
}
